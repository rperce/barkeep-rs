import shared from '../support/mocha-shared'
import { describePath } from '../support/util'
shared.behavior('has the navbar', () => {
  it('has the navbar', () => {
    cy.findByRole('navigation').within(() => {
      cy.findByText('Cocktails').should('exist')
      cy.findByText('Ingredients').should('exist')
      cy.findByText('Login').should('exist')
    })
  })
})

describePath('/cocktails', () => {
  shared.behavior('has the navbar')
})

describePath('/cocktails/all', () => {
  shared.behavior('has the navbar')
})

shared.behavior('has an ingredients table', () => {
  it('has an ingredients table', () => {
    cy.findByRole('table').within(() => {
      cy.findByRole('columnheader', { name: /ingredient/i }).should('exist')
    })
  })
})

describePath('/ingredients', () => {
  shared.behavior('has the navbar')

  shared.behavior('has an ingredients table')
})

describePath('/cocktail/example', () => {
  shared.behavior('has the navbar')

  it('is titled the cocktail name', () => {
    cy.findByRole('heading', { name: /example/ }).should('exist')
  })
})

describePath('/cocktail/example/edit', () => {
  shared.behavior('has the navbar')

  it('has an input box for the name', () => {
    cy.findByDisplayValue('example').should('exist')
  })
})
