import shared from '../support/mocha-shared'

shared.behavior('serves some html', (url: string) => {
  it('serves some html', () => {
    cy.request(url).then(resp => {
      expect(resp.headers).to.have.property('content-type')
      expect(resp.headers['content-type']).to.include('text/html')
    })
  })
})

shared.behavior('is not found', (url: string) => {
  it('is not found', () => {
    cy.request({ url, failOnStatusCode: false }).then(resp => {
      expect(resp.status).to.eq(404)
    })
  })
})

shared.behavior('only loads on internal IPs', (url: string) => {
  it('only loads on internal IPs', () => {
    cy.request(url).then(resp => {
      expect(resp.status).to.eq(200)
    })

    cy.request({
      url,
      headers: {
        'X-Real-IP': '1.3.3.7',
      },
      failOnStatusCode: false,
    }).then(resp => {
      expect(resp.status).to.eq(404)
    })
  })
})

shared.behavior('404s from external IPs', (_dummy: '', request: typeof cy.request) => {
  it('404s from external IPs', () => {
    request({ headers: { 'X-Real-IP': '1.2.3.4' } }).then(resp => {
      expect(resp.status).to.eq(404)
    })
  })
})
shared.behavior('400s for malformed json', (_dummy: '', request: typeof cy.request) => {
  it('400s for malformed json', () => {
    request({ body: '{bogus json]' }).then(resp => {
      expect(resp.status).to.eq(400)
    })
  })
})
shared.behavior('422s for unsemantic json', (_dummy: '', request: typeof cy.request) => {
  it('422s for unsemantic json', () => {
    request({ body: '{"valid": "json", "that": "means nothing"}' }).then(resp => {
      expect(resp.status).to.eq(422)
    })
  })
})

describe('api', () => {
  describe('GET /cocktails', () => {
    shared.behavior('serves some html', '/cocktails')
  })

  describe('GET /cocktails/all', () => {
    shared.behavior('serves some html', '/cocktails/all')
  })

  describe('GET /', () => {
    it('redirects to /cocktails', () => {
      cy.request({ url: '/', followRedirect: false }).then(resp => {
        expect(resp.status).to.eq(303)
        expect(resp.redirectedToUrl).to.match(/\/cocktails$/)
      })
    })
  })

  describe('GET /cocktail/<name>', () => {
    context('the cocktail exists', () => {
      shared.behavior('serves some html', '/cocktail/example')
    })
    context('the cocktail does not exist', () => {
      shared.behavior('is not found', `/cocktail/test-id-${Math.random()}`)
    })
  })

  describe('GET /cocktail/<name>/edit', () => {
    context('with internal IP', () => {
      const request = (url: string) =>
        cy.request({ url, headers: { 'X-Real-IP': '192.168.0.42' }, failOnStatusCode: false })

      it('200s with some html for existing name', () => {
        request('/cocktail/example/edit').then(resp => {
          expect(resp.status).to.eq(200)
          expect(resp.headers).to.have.property('content-type')
          expect(resp.headers['content-type']).to.include('text/html')
        })
      })
      it('404s for nonexistent name', () => {
        request(`/cocktail/test-id-${Math.random()}`).then(resp => {
          expect(resp.status).to.eq(404)
        })
      })
    })
    context('with external IP', () => {
      const request = (url: string) =>
        cy.request({ url, headers: { 'X-Real-IP': '1.2.3.4' }, failOnStatusCode: false })

      it('404s for existing name', () => {
        request('/cocktail/example/edit').then(resp => {
          expect(resp.status).to.eq(404)
        })
      })
      it('404s for nonexistent name', () => {
        request(`/cocktail/test-id-${Math.random()}`).then(resp => {
          expect(resp.status).to.eq(404)
        })
      })
    })
  })
  describe('GET /ingredients', () => {
    shared.behavior('only loads on internal IPs', '/ingredients')
    shared.behavior('serves some html', '/ingredients')
  })
  describe('GET /create_cocktail', () => {
    shared.behavior('only loads on internal IPs', '/create_cocktail')
    shared.behavior('serves some html', '/create_cocktail')
  })
  describe('POST /ingredients', () => {
    const request = (args: Partial<Parameters<typeof cy.request>[0]>) =>
      cy.request({
        url: '/ingredients',
        method: 'POST',
        headers: { 'X-Real-IP': '192.168.0.123', 'Content-Type': 'application/json' },
        failOnStatusCode: false,
        ...args,
      })

    shared.behavior('404s from external IPs', '_', request)
    shared.behavior('400s for malformed json', '_', request)
    shared.behavior('422s for unsemantic json', '_', request)
  })
  describe('POST /cocktail/edit', () => {
    const request = (args: Partial<Parameters<typeof cy.request>[0]>) =>
      cy.request({
        url: '/cocktail/edit',
        method: 'POST',
        headers: { 'X-Real-IP': '192.168.0.123', 'Content-Type': 'application/json' },
        failOnStatusCode: false,
        ...args,
      })

    shared.behavior('404s from external IPs', '_', request)
    shared.behavior('400s for malformed json', '_', request)
    shared.behavior('422s for unsemantic json', '_', request)
    it('409s for conflicting name', () => {
      request({
        body: {
          from: 'example',
          name: 'Margarita',
          instructions: '',
          recipe: [],
        },
      }).then(resp => {
        expect(resp.status).to.eq(409)
      })
    })
  })
  describe('POST /create_cocktail', () => {
    const request = (args: Partial<Parameters<typeof cy.request>[0]>) =>
      cy.request({
        url: '/create_cocktail',
        method: 'POST',
        headers: { 'X-Real-IP': '192.168.0.123', 'Content-Type': 'application/json' },
        failOnStatusCode: false,
        ...args,
      })
    shared.behavior('404s from external IPs', '_', request)
    shared.behavior('400s for malformed json', '_', request)
    shared.behavior('422s for unsemantic json', '_', request)
    it('409s for conflicting name', () => {
      request({
        body: {
          name: 'Margarita',
          instructions: '',
          recipe: [],
        },
      }).then(resp => {
        expect(resp.status).to.eq(409)
      })
    })
  })
})
