import { describePath } from '../support/util'
describePath('/cocktail/example/edit', () => {
  const nameInputs = () => cy.findAllByRole('textbox', { name: 'name' })
  it('can add ingredients', () => {
    nameInputs().should('have.length', 1)
    nameInputs().eq(0).should('have.value', 't t t')

    cy.findByRole('button', { name: /add ingredient/i }).click()

    nameInputs().should('have.length', 2)
    nameInputs().eq(0).should('have.value', 't t t')
    nameInputs().eq(1).should('have.value', '')

    nameInputs().eq(1).type('second', { delay: 1 })

    cy.findByRole('button', { name: /add ingredient/i }).click()
    nameInputs().should('have.length', 3)
    nameInputs().eq(2).type('third', { delay: 1 })

    nameInputs().eq(0).should('have.value', 't t t')
    nameInputs().eq(1).should('have.value', 'second')
    nameInputs().eq(2).should('have.value', 'third')
  })

  it('can edit instructions', () => {
    cy.findAllByRole('textbox', { name: /instructions/i }).clear().type('hello');
    cy.findAllByRole('textbox', { name: /instructions/i }).should('have.value', 'hello');
  });

  describe('can delete ingredients', () => {
    beforeEach(() => {
      cy.findByRole('button', { name: /add ingredient/i }).click()
      cy.findByRole('button', { name: /add ingredient/i }).click()

      nameInputs().eq(0).clear().type('first', { delay: 0 })
      nameInputs().eq(1).clear().type('second', { delay: 0 })
      nameInputs().eq(2).clear().type('third', { delay: 0 })
    })

    it('can delete the first ingredient', () => {
      cy.findAllByRole('button', { name: /delete/i }).eq(0).click()
      nameInputs().should('have.length', 2)
      nameInputs().eq(0).should('have.value', 'second')
      nameInputs().eq(1).should('have.value', 'third')
    })

    it('can delete a middle ingredient', () => {
      cy.findAllByRole('button', { name: /delete/i }).eq(1).click()
      nameInputs().should('have.length', 2)
      nameInputs().eq(0).should('have.value', 'first')
      nameInputs().eq(1).should('have.value', 'third')
    })

    it('can delete the last ingredient', () => {
      cy.findAllByRole('button', { name: /delete/i }).eq(2).click()
      nameInputs().should('have.length', 2)
      nameInputs().eq(0).should('have.value', 'first')
      nameInputs().eq(1).should('have.value', 'second')
    })

    it('can delete all ingredients', () => {
      cy.findAllByRole('button', { name: /delete/i }).eq(0).click()
      cy.findAllByRole('button', { name: /delete/i }).eq(0).click()
      cy.findAllByRole('button', { name: /delete/i }).eq(0).click()
      nameInputs().should('have.length', 0)
    })
  })
})
