export const describePath = (path: string, fn: () => void) => {
  describe(path, () => {
    beforeEach(() => cy.visitPath(path));
    fn();
  });
}
