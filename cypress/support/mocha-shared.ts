type Description = string;
type BehaviorFn = (...args: any[]) => void;

class BehaviorNotDefinedError extends Error {
  constructor(description: string) {
    super(`No shared behavior defined that matches description [${description}].`);
    Object.setPrototypeOf(this, BehaviorNotDefinedError.prototype);
  }
}

const behaviors = new Map<Description, BehaviorFn>();

function getOrSet(description: Description, ...args: any[]) {
  if (args[0] && typeof(args[0]) === 'function') {
    behaviors.set(description, args[0]);
  } else {
    const fn = behaviors.get(description)
    if (fn) {
      fn(...args);
    } else {
      throw new BehaviorNotDefinedError(description);
    }
  }
}

export default {
  behavior: getOrSet,
};
