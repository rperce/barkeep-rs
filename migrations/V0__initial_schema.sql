PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS cocktail (
    id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    instructions TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS ingredient (
  name TEXT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS stock (
  ingredient TEXT NOT NULL REFERENCES ingredient(name) ON DELETE cascade
);

CREATE TABLE IF NOT EXISTS ingredient_type (
  id TEXT NOT NULL PRIMARY KEY
);
INSERT INTO ingredient_type (id)
VALUES ('Measured'), ('Muddled'), ('ToFill'), ('Garnish')
ON CONFLICT DO NOTHING;

CREATE TABLE IF NOT EXISTS recipe_entry (
  cocktail TEXT NOT NULL REFERENCES cocktail(id) ON DELETE cascade,
  ingredient TEXT NOT NULL REFERENCES ingredient(name),
  type TEXT NOT NULL REFERENCES ingredient_type(id),
  measure TEXT
);
