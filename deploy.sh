#!/usr/bin/env sh
die() { >&2 printf "Error: %s" "$*"; exit 1; }
if [ "$(hostname)" != "xalicas" ]; then
    die "this script must be run on xalicas. You probably meant to run './Taskfile deploy'."
fi

if [ ! -f barkeep ]; then
    die "'barkeep' executable is missing."
fi

if [ ! -x barkeep ]; then
    die "'barkeep' is not an executable file."
fi

if [ ! -d public ]; then
    die "'public' directory is missing."
fi

set -x
sudo systemctl stop barkeep
sudo rm -rf /barkeep/public /barkeep/templates
for file in barkeep public templates; do
    sudo mv "$file" /barkeep/
done

cat <<EOF | sudo tee /barkeep/Rocket.toml > /dev/null
[release]
secret_key = "$(openssl rand -base64 32)"
EOF

sudo chown -R http:http /barkeep
sudo systemctl start barkeep
