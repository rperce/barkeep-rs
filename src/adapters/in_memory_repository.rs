use crate::domain::entities::Ingredient;
use crate::domain::entities::{Cocktail, CocktailName};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
// use std::fs;

use crate::domain::repository::{
    AliasError, CocktailWithReady, GetError, InsertError, Inventory, ReplaceError, Repository,
    StockError,
};

pub struct InMemoryRepository {
    cocktails: HashMap<CocktailName, Cocktail>,
    stock: HashSet<Ingredient>,
    aliases: HashMap<Ingredient, Ingredient>,
}

#[derive(Serialize, Deserialize)]
struct StoreJson {
    cocktails: Vec<Cocktail>,
    inventory: Vec<Ingredient>,
}
impl InMemoryRepository {
    #[cfg(test)]
    pub fn new() -> Self {
        Self {
            cocktails: HashMap::new(),
            stock: HashSet::new(),
            aliases: HashMap::new(),
        }
    }
    fn ingredients(&self) -> HashSet<Ingredient> {
        self.cocktails
            .values()
            .flat_map(|c| c.recipe.iter().map(|r| r.ingredient.clone()))
            .collect()
    }
}

impl Repository for InMemoryRepository {
    fn insert(&mut self, cocktail: Cocktail) -> Result<(), InsertError> {
        let name = cocktail.name.canonicalize();
        if self.cocktails.contains_key(&name) {
            return Err(InsertError::Conflict);
        }

        self.cocktails.insert(name, cocktail);
        Ok(())
    }

    fn get_cocktail(&self, name: &CocktailName) -> Result<CocktailWithReady, GetError> {
        match self.cocktails.get(&name.canonicalize()) {
            Some(c) => Ok(c.with_ready(self)),
            None => Err(GetError::NotFound),
        }
    }

    fn replace(&mut self, name: &CocktailName, cocktail: Cocktail) -> Result<(), ReplaceError> {
        if let Err(GetError::NotFound) = self.get_cocktail(name) {
            return Err(ReplaceError::NotFound);
        }

        self.cocktails.remove(&name.canonicalize());
        self.insert(cocktail).map_err(|e| match e {
            InsertError::Conflict => ReplaceError::Conflict,
        })?;

        Ok(())
    }

    fn inventory(&self, ready_only: bool) -> HashSet<Inventory> {
        self.ingredients()
            .iter()
            .map(|i| i.stocked(self))
            .filter(|i| if ready_only { i.stocked } else { true })
            .collect()
    }

    fn cocktails(&self, ready_only: bool) -> HashSet<CocktailWithReady> {
        self.cocktails
            .values()
            .filter(|c| {
                if ready_only {
                    self.is_ready(&c.name)
                } else {
                    true
                }
            })
            .map(|c| c.with_ready(self))
            .collect()
    }

    fn set_stocked(&mut self, ing: &Ingredient, have: bool) -> Result<(), StockError> {
        if !self.ingredients().contains(ing) {
            return Err(StockError::NotFound);
        }

        match have {
            true => self.stock.insert(ing.clone()),
            false => self.stock.remove(&ing),
        };

        Ok(())
    }

    fn is_stocked(&self, ing: &Ingredient) -> bool {
        self.stock.contains(self.aliases.get(ing).unwrap_or(ing))
    }

    fn is_ready(&self, name: &CocktailName) -> bool {
        match self.cocktails.get(&name.canonicalize()) {
            Some(c) => c.recipe.iter().all(|r| self.is_stocked(&r.ingredient)),
            None => false,
        }
    }

    fn alias(&mut self, ing: &Ingredient, stock_source: &Ingredient) -> Result<(), AliasError> {
        let ings = self.ingredients();
        if !ings.contains(ing) || !ings.contains(stock_source) {
            return Err(AliasError::NotFound);
        }

        self.aliases.insert(ing.clone(), stock_source.clone());
        Ok(())
    }
}
