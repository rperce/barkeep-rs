use crate::domain::entities::{
    Cocktail, CocktailName, Ingredient, IngredientType, Measure, RecipeEntry,
};
use indoc::indoc;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::fs;

use crate::domain::repository::{
    AliasError, CocktailWithReady, GetError, InsertError, Inventory, ReplaceError, Repository,
    StockError,
};

use r2d2::PooledConnection;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{params, Error::SqliteFailure, OptionalExtension, Result};
mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("./migrations");
}

use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize)]
struct StoreJson {
    cocktails: Vec<Cocktail>,
    inventory: Vec<Ingredient>,
}

pub struct SQLiteRepository {
    pool: r2d2::Pool<SqliteConnectionManager>,
}
impl SQLiteRepository {
    pub fn new(uri: &str) -> Self {
        let manager = SqliteConnectionManager::file(uri).with_init(|c| {
            embedded::migrations::runner().run(c).unwrap();
            Ok(())
        });
        let pool = r2d2::Pool::new(manager).unwrap();
        let out = Self { pool };
        out
    }

    pub fn from_test_fixtures(uri: &str) -> Result<Self, String> {
        let data = fs::read_to_string("tests/cocktails.json").unwrap();
        let json: StoreJson = serde_json::from_str(&data).map_err(|e| e.to_string())?;

        let mut repo = Self::new(uri);

        for c in json.cocktails.into_iter() {
            repo.insert(c).map_err(|e| format!("{:?}", e))?;
        }

        for i in json.inventory.into_iter() {
            repo.set_stocked(&i, true)
                .map_err(|e| format!("{:?} {:?}", e, i))?;
        }

        Ok(repo)
    }

    pub fn connection(&self) -> PooledConnection<SqliteConnectionManager> {
        let conn = self.pool.get().unwrap();
        conn.pragma_update(None, "foreign_keys", &"ON").unwrap();
        conn
    }

    fn get_cocktail_in_connection(
        &self,
        name: &CocktailName,
        conn: &PooledConnection<SqliteConnectionManager>,
    ) -> Result<CocktailWithReady, GetError> {
        let id = name.canonicalize().0;
        let mut statement = conn
            .prepare("select name, instructions from cocktail where id = ?")
            .unwrap();
        let name_and_instructions = statement
            .query_row(params![id], |row| {
                let name: String = row.get(0)?;
                let instructions: String = row.get(1)?;
                Ok((name, instructions))
            })
            .optional()
            .unwrap();
        match name_and_instructions {
            None => Err(GetError::NotFound),
            Some((name, instructions)) => {
                let mut get_recipe = conn
                    .prepare(indoc! {r#"
                select distinct
                    r.ingredient, r.type, r.measure,
                    case when s.ingredient is null then 0 else 1 end as stocked
                from
                    recipe_entry r
                    left join stock s on r.ingredient = s.ingredient
                where
                    r.cocktail = ?"#})
                    .unwrap();

                let recipe_parts = get_recipe
                    .query_map(params![id], |r| {
                        Ok((r.get(0)?, r.get(1)?, r.get(2)?, r.get(3)?))
                    })
                    .unwrap()
                    .map(|row| {
                        let (ing, r#type, measure, stocked): (
                            String,
                            String,
                            Option<String>,
                            bool,
                        ) = row.unwrap();
                        (ing, r#type, measure, stocked)
                    })
                    .collect::<Vec<(String, String, Option<String>, bool)>>();

                let mut ready = true;
                let recipe = recipe_parts
                    .iter()
                    .map(|(ing, r#type, measure, stocked)| {
                        ready = ready && *stocked;
                        RecipeEntry::from((
                            ing.clone(),
                            IngredientType::try_from(r#type.clone()).unwrap(),
                            match measure {
                                Some(s) => Some(Measure(s.clone())),
                                None => None,
                            },
                        ))
                    })
                    .collect();

                Ok(CocktailWithReady {
                    name: CocktailName::from(name.clone()),
                    instructions,
                    recipe,
                    ready,
                })
            }
        }
    }

    fn insert_in_connection(
        &mut self,
        c: Cocktail,
        conn: &PooledConnection<SqliteConnectionManager>,
    ) -> Result<(), InsertError> {
        let canonical = c.name.clone().canonicalize().0;
        let mut check = conn.prepare("select * from cocktail where id = ?").unwrap();
        if check.exists(params![canonical.clone()]).unwrap() {
            return Err(InsertError::Conflict);
        }

        let mut insert_cocktail = conn
            .prepare("insert into cocktail (id, name, instructions) values (?, ?, ?)")
            .unwrap();
        let mut insert_ingredient = conn
            .prepare("insert into ingredient (name) values (?) on conflict do nothing;")
            .unwrap();
        let mut insert_entry = conn
            .prepare(indoc! {r#"
                insert into recipe_entry (cocktail, ingredient, type, measure)
                    values (?, ?, ?, ?);
            "#})
            .unwrap();

        insert_cocktail
            .execute(params![
                canonical.as_str(),
                c.name.clone().0.as_str(),
                c.instructions
            ])
            .unwrap();

        for entry in c.recipe {
            let measure = match entry.measure {
                Some(m) => Some(m.0.clone()),
                None => None,
            };
            insert_ingredient
                .execute(params![entry.ingredient.0])
                .unwrap();
            insert_entry
                .execute(params![
                    c.name.canonicalize().0.clone(),
                    entry.ingredient.0.clone(),
                    entry.r#type.to_string(),
                    measure.clone()
                ])
                .unwrap();
        }
        Ok(())
    }
}

impl Repository for SQLiteRepository {
    fn get_cocktail(&self, name: &CocktailName) -> Result<CocktailWithReady, GetError> {
        let conn = self.connection();
        self.get_cocktail_in_connection(name, &conn)
    }

    fn inventory(&self, ready_only: bool) -> std::collections::HashSet<Inventory> {
        let conn = self.connection();
        if ready_only {
            let mut statement = conn.prepare("select * from stock").unwrap();
            return statement
                .query_map([], |r| r.get(0))
                .unwrap()
                .map(|e| Inventory {
                    ingredient: Ingredient(e.unwrap()),
                    stocked: true,
                })
                .collect::<HashSet<Inventory>>();
        }

        let mut stmt = conn
            .prepare(indoc! {r#"
            select
                i.name as ingredient,
                case when s.ingredient is null then 0 else 1 end as stocked
            from
                ingredient i
                left join stock s on i.name = s.ingredient
            "#})
            .unwrap();
        return stmt
            .query_map([], |r| Ok((r.get(0)?, r.get(1)?)))
            .unwrap()
            .map(|res| {
                let (ing, sto): (String, bool) = res.unwrap();
                Inventory {
                    ingredient: Ingredient(ing),
                    stocked: sto,
                }
            })
            .collect::<HashSet<Inventory>>();
    }

    fn cocktails(&self, ready_only: bool) -> std::collections::HashSet<CocktailWithReady> {
        let conn = self.connection();
        let cocktails = conn
            .prepare("select name from cocktail c")
            .unwrap()
            .query_map([], |r| r.get(0))
            .unwrap()
            .filter_map(|name: Result<String>| {
                let cocktail = self
                    .get_cocktail_in_connection(&CocktailName::from(name.unwrap().as_str()), &conn);
                match cocktail {
                    Ok(cocktail) => {
                        if !ready_only || cocktail.ready {
                            Some(cocktail)
                        } else {
                            None
                        }
                    }
                    Err(GetError::NotFound) => None,
                }
            })
            .collect::<HashSet<CocktailWithReady>>();
        cocktails
    }

    fn is_stocked(&self, ingredient: &Ingredient) -> bool {
        let connection = self.connection();
        let mut statement = connection
            .prepare("select * from stock where ingredient = ?")
            .unwrap();
        statement.exists(params![ingredient.0.clone()]).unwrap()
    }

    fn is_ready(&self, name: &CocktailName) -> bool {
        let conn = self.connection();
        let mut stmt = conn
            .prepare(indoc! {r#"
                select
                    case when s.ingredient is null then 0 else 1 end,
                    r.ingredient
                from
                    recipe_entry r
                    left join stock s on r.ingredient = s.ingredient
                where
                    r.cocktail = ?
            "#})
            .unwrap();
        let ready: bool = stmt
            .query_map(params![name.canonicalize().0], |row| row.get(0))
            .unwrap()
            .fold(true, |ready, each| ready && each.unwrap());
        ready
    }

    fn insert(&mut self, c: Cocktail) -> Result<(), InsertError> {
        let conn = self.connection();
        conn.execute("BEGIN TRANSACTION", []).unwrap();
        let out = self.insert_in_connection(c, &conn);
        conn.execute("COMMIT", []).unwrap();

        out
    }

    fn replace(&mut self, name: &CocktailName, c: Cocktail) -> Result<(), ReplaceError> {
        let conn = self.connection();
        conn.execute("BEGIN TRANSACTION", []).unwrap();
        let id = name.canonicalize().0;
        let mut remove = conn
            .prepare("delete from cocktail where id = ? returning id")
            .unwrap();
        let deleted: Option<String> = remove
            .query_row(params![id], |row| row.get(0))
            .optional()
            .unwrap();
        if deleted == None {
            return Err(ReplaceError::NotFound);
        }

        self.insert_in_connection(c, &conn).map_err(|e| {
            conn.execute("ROLLBACK", []).unwrap();
            match e {
                InsertError::Conflict => ReplaceError::Conflict,
            }
        })?;

        conn.execute(
            "delete from ingredient where name not in (select distinct ingredient from recipe_entry)", [])
            .unwrap();

        conn.execute("COMMIT", []).unwrap();
        Ok(())
    }

    fn set_stocked(&mut self, ingredient: &Ingredient, have: bool) -> Result<(), StockError> {
        let conn = self.connection();
        let mut stmt = match have {
            true => conn
                .prepare("insert into stock (ingredient) values (?) on conflict do nothing")
                .unwrap(),
            false => conn
                .prepare("delete from stock where ingredient = ?")
                .unwrap(),
        };
        stmt.execute(params![ingredient.0]).map_err(|e| match e {
            SqliteFailure(_, Some(ref msg)) => match msg.as_str() {
                "FOREIGN KEY constraint failed" => StockError::NotFound,
                _ => panic!("{}", e),
            },
            _ => panic!("{}", e),
        })?;

        Ok(())
    }

    fn alias(
        &mut self,
        _ingredient: &Ingredient,
        _stock_source: &Ingredient,
    ) -> Result<(), AliasError> {
        todo!()
    }
}
