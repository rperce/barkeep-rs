use rocket::form::Form;
use rocket::http::Status;
use rocket::tokio::sync::RwLock;
use rocket_auth::{Auth, Login, Signup, User};
use std::collections::HashMap;

use ipnet::Ipv4Net;
use iprange::IpRange;
use rocket::response::Redirect;
use rocket::serde::json::Json;
use rocket::State;
use rocket_dyn_templates::Template;
use std::net::IpAddr;

use crate::domain::actions::{
    create_cocktail, edit_cocktail, get_inventory, list_cocktails, update_stock,
};
use crate::domain::actions::{
    Action, ActionMut, CreateCocktail, EditCocktail, GetInventory, ListCocktails, UpdateStock,
};
use crate::domain::entities::{Cocktail, CocktailName, Ingredient};
use crate::domain::repository::{CocktailWithReady, GetError, Inventory, Repository};

#[derive(Debug)]
pub struct NginxRealIP(IpAddr);
#[rocket::async_trait]
impl<'r> rocket::request::FromRequest<'r> for NginxRealIP {
    type Error = ();
    async fn from_request(
        request: &'r rocket::Request<'_>,
    ) -> rocket::request::Outcome<Self, Self::Error> {
        match request.real_ip() {
            Some(ip) => rocket::request::Outcome::Success(NginxRealIP(ip)),
            None => {
                println!("WARNING: no X-Real-IP header; falling back to actual request ip");
                match request.client_ip() {
                    Some(ip) => rocket::request::Outcome::Success(NginxRealIP(ip)),
                    None => rocket::request::Outcome::Failure((
                        rocket::http::Status::from_code(401).unwrap(),
                        (),
                    )),
                }
            }
        }
    }
}

pub struct Repo {
    pub repo: RwLock<Box<dyn Repository>>,
}

macro_rules! collection {
    // map-like
    ($($k:expr => $v:expr),* $(,)?) => {{
        use std::iter::{Iterator, IntoIterator};
        Iterator::collect(IntoIterator::into_iter([$(($k, $v),)*]))
    }};
    // set-like
    ($($v:expr),* $(,)?) => {{
        use std::iter::{Iterator, IntoIterator};
        Iterator::collect(IntoIterator::into_iter([$($v,)*]))
    }};
}

fn local_ip() -> IpRange<Ipv4Net> {
    ["192.168.1.0/24", "192.168.0.0/24", "127.0.0.0/24"]
        .iter()
        .map(|s| s.parse().unwrap())
        .collect()
}

type Error<R> = (rocket::http::Status, R);
type WResult<R> = core::result::Result<R, Error<String>>;
fn map_serde_err(e: serde_json::Error) -> Error<String> {
    match e.classify() {
        serde_json::error::Category::Io => (Status::InternalServerError, "IO Failure".to_string()),
        serde_json::error::Category::Syntax => {
            (Status::InternalServerError, "Syntax failure".to_string())
        }
        serde_json::error::Category::Data => {
            (Status::InternalServerError, "Data failure".to_string())
        }
        serde_json::error::Category::Eof => {
            (Status::InternalServerError, "Unexpected EOF".to_string())
        }
    }
}
async fn cocktails_json(repo_mutex: &RwLock<Box<dyn Repository>>) -> WResult<String> {
    let repo = repo_mutex.read().await;

    Ok(serde_json::to_string(
        &ListCocktails::exec(&**repo, list_cocktails::Request { ready_only: false })
            .unwrap()
            .iter()
            .collect::<Vec<&CocktailWithReady>>(),
    )
    .map_err(map_serde_err)?)
}

fn cocktail_json(cocktail: &Cocktail) -> WResult<String> {
    serde_json::to_string(cocktail).map_err(map_serde_err)
}

async fn inventory_json(repo_mutex: &RwLock<Box<dyn Repository>>) -> WResult<String> {
    let repo = repo_mutex.read().await;

    serde_json::to_string(
        &GetInventory::exec(&**repo, get_inventory::Request { ready_only: false })
            .unwrap()
            .iter()
            .map(|each| each.clone())
            .collect::<Vec<Inventory>>(),
    )
    .map_err(map_serde_err)
}

async fn stock_json(repo_mutex: &RwLock<Box<dyn Repository>>) -> Result<String, String> {
    let repo = repo_mutex.read().await;

    Ok(serde_json::to_string(
        &GetInventory::exec(&**repo, get_inventory::Request { ready_only: true })
            .unwrap()
            .iter()
            .map(|each| each.ingredient.clone())
            .collect::<Vec<Ingredient>>(),
    )
    .map_err(|e| e.to_string())?)
}

#[get("/cocktails")]
pub async fn get_cocktails(state: &State<Repo>, user: Option<User>) -> Result<Template, String> {
    let context: HashMap<_, _> = collection! {
        "title"     => String::from("Cocktails"),
        "js"        => String::from("cocktails"),
        "cocktails" => cocktails_json(&state.repo).await.map_err(|e|format!("{:?}",e))?,
        "stock"     => stock_json(&state.repo).await?,
        "logged_in" => user.map_or(false.to_string(), |_| true.to_string()),
    };

    Ok(Template::render("cocktails", &context))
}

#[get("/cocktails/all")]
pub async fn get_cocktails_all(
    state: &State<Repo>,
    user: Option<User>,
) -> Result<Template, String> {
    get_cocktails(state, user).await
}

#[get("/")]
pub fn index() -> Redirect {
    Redirect::to(uri!(get_cocktails))
}

#[get("/cocktail/<name>")]
pub async fn cocktail(name: &str, state: &State<Repo>, user: Option<User>) -> WResult<Template> {
    let cocktail = state
        .repo
        .read()
        .await
        .get_cocktail(&CocktailName::from(name))
        .map_err(|e| match e {
            GetError::NotFound => (Status::NotFound, "Not Found".to_string()),
        })?;

    let context: HashMap<_, _> = collection! {
        "title"    => cocktail.name.0.clone(),
        "js"       => String::from("cocktail"),
        "cocktail" => serde_json::to_string(&cocktail).map_err(map_serde_err)?,
        "logged_in" => user.map_or(false.to_string(), |_| true.to_string()),
    };

    Ok(Template::render("cocktail", &context))
}

#[get("/cocktail/<name>/edit")]
pub async fn cocktail_edit(_user: User, name: &str, state: &State<Repo>) -> WResult<Template> {
    let cocktail = state
        .repo
        .write()
        .await
        .get_cocktail(&CocktailName::from(name))
        .map_err(|e| match e {
            GetError::NotFound => (Status::NotFound, "Not Found".to_string()),
        })?;

    let context: HashMap<_, _> = collection! {
        "title"     => cocktail.name.0.clone(),
        "js"        => String::from("edit"),
        "cocktail"  => serde_json::to_string(&cocktail).map_err(map_serde_err)?,
        "logged_in" => "true".to_string(),
    };

    // purposefully using cocktail template, just different js arg in context
    Ok(Template::render("cocktail", &context))
}

#[get("/ingredients")]
pub async fn ingredients(user: Option<User>, state: &State<Repo>) -> WResult<Template> {
    let context: HashMap<_, _> = collection! {
            "title" => String::from("Ingredients"),
            "js" => String::from("ingredients"),
            "cocktails" => cocktails_json(&state.repo).await?,
            "inventory" => inventory_json(&state.repo).await?,
    "logged_in" => user.map_or(false.to_string(), |_| true.to_string()),
        };

    Ok(Template::render("ingredients", &context))
}
#[post("/ingredients", format = "json", data = "<data>")]
pub async fn post_ingredients(
    _user: User,
    data: Json<update_stock::Request>,
    state: &State<Repo>,
) -> WResult<()> {
    let mut repo = state.repo.write().await;
    UpdateStock::exec_mut(&mut repo, data.into_inner()).map_err(|e| match e {
        update_stock::Error::NotFound => (Status::NotFound, "Ingredient not found".to_string()),
    })?;

    Ok(())
}

#[post("/cocktail/edit", format = "json", data = "<data>")]
pub async fn post_cocktail_edit(
    _user: User,
    data: Json<edit_cocktail::Request>,
    state: &State<Repo>,
) -> WResult<()> {
    let mut repo = state.repo.write().await;
    let req = data.into_inner();
    EditCocktail::exec_mut(&mut repo, req.clone()).map_err(|e| match e {
        edit_cocktail::Error::NotFound => (Status::NotFound, "Not Found".to_string()),
        edit_cocktail::Error::Conflict => (
            Status::Conflict,
            format!(
                "a cocktail already exists with name '{}'",
                req.name.map(|x| x.0).unwrap_or("".to_string())
            ),
        ),
    })?;

    Ok(())
}

#[get("/create_cocktail")]
pub fn cocktail_create(_user: User) -> WResult<Template> {
    let context: HashMap<_, _> = collection! {
        "title" => String::from("New cocktail"),
        "js" => String::from("edit"),
        "cocktail" => cocktail_json(&Cocktail::new(String::from(""), String::from(""), vec![]))?,
        "logged_in" => "true".to_string(),
    };

    return Ok(Template::render("cocktail", &context));
}

#[post("/create_cocktail", format = "json", data = "<data>")]
pub async fn post_cocktail_create(
    _user: User,
    data: Json<create_cocktail::Request>,
    state: &State<Repo>,
) -> WResult<()> {
    let mut repo = state.repo.write().await;
    let req = data.into_inner();
    CreateCocktail::exec_mut(&mut repo, req.clone()).map_err(|e| match e {
        create_cocktail::Error::Conflict => (
            Status::Conflict,
            format!("a cocktail already exists with name '{}'", req.name),
        ),
    })?;

    return Ok(());
}

#[get("/signup")]
pub async fn get_signup(real_ip: NginxRealIP) -> WResult<Template> {
    if let IpAddr::V4(ip) = real_ip.0 {
        if local_ip().contains(&Ipv4Net::from(ip)) {
            let context: HashMap<_, _> = collection! {
                "js" => String::from("Signup"),
            };
            return Ok(Template::render("signup", &context));
        }
    }
    println!("Rejecting access to GET /signup from {:?}", real_ip);
    Err((Status::NotFound, "Contact rperce for signup".to_string()))
}

#[post("/signup", data = "<form>")]
pub async fn post_signup(
    real_ip: NginxRealIP,
    auth: Auth<'_>,
    form: Form<Signup>,
) -> Result<Redirect, rocket_auth::prelude::Error> {
    if let IpAddr::V4(ip) = real_ip.0 {
        if local_ip().contains(&Ipv4Net::from(ip)) {
            auth.signup(&form).await?;
            auth.login(&form.into()).await?;
        }
    } else {
        println!("Rejecting access to POST /signup from {:?}", real_ip);
    }
    Ok(Redirect::to("/"))
}

#[get("/login")]
pub fn get_login() -> WResult<Template> {
    let context: HashMap<_, _> = collection! {
        "js" => String::from("Login"),
    };
    return Ok(Template::render("signup", &context));
}

#[post("/login", data = "<form>")]
pub async fn post_login(
    auth: Auth<'_>,
    form: Form<Login>,
) -> Result<Redirect, rocket_auth::prelude::Error> {
    let result = auth.login(&form).await;
    println!("login attempt: {:?}", result);
    result?;
    Ok(Redirect::to("/"))
}

#[get("/logout")]
pub fn get_logout(auth: Auth<'_>) -> Result<Redirect, rocket_auth::prelude::Error> {
    auth.logout()?;
    Ok(Redirect::to("/"))
}
