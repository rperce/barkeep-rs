#[macro_use]
extern crate rocket;

use rocket::fs::FileServer;
use rocket::tokio::sync::RwLock;
use rocket_auth::{prelude::Error, Users};
use rocket_dyn_templates::Template;
use std::env;

mod adapters;
mod domain;

use crate::adapters::{webserver::*, SQLiteRepository};

#[rocket::main]
async fn main() -> Result<(), Error> {
    let path = match env::args().nth(1) {
        None => {
            println!("No path specified, using :memory:");
            String::from(":memory:")
        }
        Some(s) => s.clone(),
    };

    let repo = match env::args().nth(2).as_ref().map(String::as_str) {
        Some("integration") => SQLiteRepository::from_test_fixtures(&path).unwrap(),
        _ => SQLiteRepository::new(path.as_str()),
    };

    let users = Users::open_rusqlite("users.db")?;

    let _ = rocket::build()
        .manage(Repo {
            repo: RwLock::new(Box::new(repo)),
        })
        .manage(users)
        .attach(Template::fairing())
        .mount("/", FileServer::from("public/"))
        .mount(
            "/",
            routes![
                index,
                get_cocktails,
                get_cocktails_all,
                cocktail,
                cocktail_edit,
                post_cocktail_edit,
                ingredients,
                post_ingredients,
                cocktail_create,
                post_cocktail_create,
                get_signup,
                post_signup,
                get_login,
                post_login,
                get_logout
            ],
        )
        .launch()
        .await
        .unwrap();
    Ok(())
}
