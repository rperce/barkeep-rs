use crate::domain::entities::{Cocktail, CocktailName, RecipeEntry};
use crate::domain::repository::{GetError, ReplaceError, Repository};
use serde::Deserialize;

use super::ActionMut;

#[derive(Deserialize, Clone)]
pub struct Request {
    pub from: CocktailName,
    pub name: Option<CocktailName>,
    pub instructions: Option<String>,
    pub recipe: Option<Vec<RecipeEntry>>,
}

#[derive(Debug, PartialEq)]
pub enum Error {
    NotFound,
    Conflict,
}

pub struct EditCocktail;
impl ActionMut for EditCocktail {
    type Request = Request;
    type Response = ();
    type Error = Error;

    fn exec_mut(
        repo: &mut Box<dyn Repository>,
        req: Self::Request,
    ) -> Result<Self::Response, Self::Error> {
        let before = repo.get_cocktail(&req.from).map_err(|e| match e {
            GetError::NotFound => Error::NotFound,
        })?;

        let name = req.name.unwrap_or(before.name).clone();
        let instructions = req.instructions.unwrap_or(before.instructions).clone();
        let recipe = req.recipe.unwrap_or(before.recipe).clone();

        repo.replace(&req.from, Cocktail::new(name, instructions, recipe))
            .map_err(|e| match e {
                ReplaceError::NotFound => Error::NotFound,
                ReplaceError::Conflict => Error::Conflict,
            })?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use super::*;

    use crate::adapters::{InMemoryRepository, SQLiteRepository};
    use crate::domain::{
        entities::{Cocktail, Ingredient, IngredientType},
        repository::{Repository, Inventory},
    };

    fn add_foo_bar(repo: &mut Box<dyn Repository>) {
        repo.insert(Cocktail::new("foo bar", "", vec![])).unwrap();
    }

    fn mem_repo() -> Box<dyn Repository> {
        Box::new(InMemoryRepository::new())
    }

    fn sql_repo() -> Box<dyn Repository> {
        Box::new(SQLiteRepository::new(":memory:"))
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn it_should_error_if_requested_id_does_not_exist(repo: &mut Box<dyn Repository>) {
        let res = EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("any"),
                name: None,
                instructions: None,
                recipe: None,
            },
        );
        assert_eq!(res, Err(Error::NotFound));
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn can_change_the_name(repo: &mut Box<dyn Repository>) {
        add_foo_bar(repo);

        EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("foo bar"),
                name: Some(CocktailName::from("Foo Bar")),
                instructions: None,
                recipe: None,
            },
        )
        .unwrap();

        EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("Foo Bar"),
                name: Some(CocktailName::from("Bazzer")),
                instructions: None,
                recipe: None,
            },
        )
        .unwrap();
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn can_change_the_instructions(repo: &mut Box<dyn Repository>) {
        add_foo_bar(repo);

        EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("foo bar"),
                name: None,
                instructions: Some(String::from("Put it in a coconut and shake it all up.")),
                recipe: None,
            },
        )
        .unwrap();

        let c = repo.get_cocktail(&CocktailName::from("foo bar")).unwrap();
        assert_eq!(c.instructions, "Put it in a coconut and shake it all up.");
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn can_change_the_ingredients(repo: &mut Box<dyn Repository>) {
        add_foo_bar(repo);

        EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("foo bar"),
                name: None,
                instructions: None,
                recipe: Some(vec![RecipeEntry {
                    ingredient: Ingredient::from("Water"),
                    r#type: IngredientType::ToFill,
                    measure: None,
                }]),
            },
        )
        .unwrap();

        let c = repo.get_cocktail(&CocktailName::from("foo bar")).unwrap();
        assert_eq!(c.recipe[0].ingredient, Ingredient::from("Water"));
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn obsolete_ingredients_are_removed(repo: &mut Box<dyn Repository>) {
        repo.insert(Cocktail::new("foo bar", "", vec![RecipeEntry {
            ingredient: Ingredient::from("Wtaer [sic]"),
            r#type: IngredientType::ToFill,
            measure: None,
        }])).unwrap();

        EditCocktail::exec_mut(
            repo,
            Request {
                from: CocktailName::from("foo bar"),
                name: None,
                instructions: None,
                recipe: Some(vec![RecipeEntry {
                    ingredient: Ingredient::from("Water"),
                    r#type: IngredientType::ToFill,
                    measure: None,
                }]),
            },
        )
        .unwrap();

        let ings = repo.inventory(false);
        assert_eq!(ings, std::collections::HashSet::from([Inventory {
            ingredient: Ingredient::from("Water"),
            stocked: false,
        }]));
    }
}
