use crate::domain::repository::Repository;

pub mod alias_ingredient;
pub mod create_cocktail;
pub use create_cocktail::CreateCocktail;

pub mod edit_cocktail;
pub use edit_cocktail::EditCocktail;

pub mod get_inventory;
pub use get_inventory::GetInventory;

pub mod update_stock;
pub use update_stock::UpdateStock;

pub mod list_cocktails;
pub use list_cocktails::ListCocktails;

pub trait Action {
    type Request;
    type Response;
    type Error;
    fn exec(repo: &dyn Repository, req: Self::Request) -> Result<Self::Response, Self::Error>;
}

pub trait ActionMut {
    type Request;
    type Response;
    type Error;
    fn exec_mut(
        repo: &mut Box<dyn Repository>,
        req: Self::Request,
    ) -> Result<Self::Response, Self::Error>;
}
