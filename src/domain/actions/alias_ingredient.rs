use crate::domain::entities::Ingredient;
use crate::domain::repository::{AliasError, Repository};

use super::ActionMut;

pub struct Request {
    ingredient: Ingredient,
    stock_source: Ingredient,
}

pub struct AliasIngredient;
impl ActionMut for AliasIngredient {
    type Request = Request;
    type Response = ();
    type Error = AliasError;

    fn exec_mut(
        repo: &mut Box<dyn Repository>,
        req: Self::Request,
    ) -> Result<Self::Response, Self::Error> {
        repo.alias(&req.ingredient, &req.stock_source)?;

        Ok(())
    }
}

#[cfg(test)]
mod test {
    // TODO: alias for SQLiteRepository
    use crate::adapters::InMemoryRepository;
    use crate::domain::entities::{Cocktail, Ingredient, IngredientType, RecipeEntry};

    use super::*;

    #[test]
    fn it_should_use_stock_of_aliased_ingredient() {
        let mut repo: Box<dyn Repository> = Box::new(InMemoryRepository::new());
        let foo = Ingredient::from("foo");
        let bar = Ingredient::from("bar");
        repo.insert(Cocktail::new(
            "c",
            "",
            vec![
                RecipeEntry::from((foo.clone(), IngredientType::ToFill)),
                RecipeEntry::from((bar.clone(), IngredientType::ToFill)),
            ],
        ))
        .unwrap();

        repo.set_stocked(&foo, true).unwrap();

        assert_eq!(repo.is_stocked(&bar), false);

        AliasIngredient::exec_mut(
            &mut repo,
            Request {
                ingredient: bar.clone(),
                stock_source: foo.clone(),
            },
        )
        .unwrap();

        assert_eq!(repo.is_stocked(&bar), true);
    }
}
