use crate::domain::repository::{CocktailWithReady, Repository};

use super::Action;

pub struct Request {
    pub ready_only: bool,
}

pub type Response = Vec<CocktailWithReady>;

pub struct ListCocktails;
impl Action for ListCocktails {
    type Request = Request;
    type Response = Response;
    type Error = ();

    fn exec(repo: &dyn Repository, req: Self::Request) -> Result<Self::Response, Self::Error> {
        let mut cocktails = repo
            .cocktails(req.ready_only)
            .into_iter()
            .map(|each| each.clone())
            .collect::<Response>();

        cocktails.sort_unstable_by(|a, b| a.name.0.cmp(&b.name.0));
        Ok(cocktails)
    }
}

#[cfg(test)]
mod test {
    use test_case::test_case;

    use crate::adapters::{InMemoryRepository, SQLiteRepository};
    use crate::domain::entities::RecipeEntry;
    use crate::domain::{
        entities::{Cocktail, Ingredient, IngredientType},
        repository::Repository,
    };

    use super::*;

    fn cocktail_a() -> Cocktail {
        Cocktail::new(
            "a",
            "",
            vec![
                RecipeEntry::from(("foo", IngredientType::Muddled)),
                RecipeEntry::from(("bar", IngredientType::ToFill)),
            ],
        )
    }

    fn cocktail_b() -> Cocktail {
        Cocktail::new(
            "b",
            "",
            vec![RecipeEntry::from(("baz", IngredientType::ToFill))],
        )
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn it_should_list_all_the_cocktails(repo: &mut dyn Repository) {
        repo.insert(cocktail_a()).unwrap();
        repo.insert(cocktail_b()).unwrap();

        let resp = ListCocktails::exec(repo, Request { ready_only: false }).unwrap();

        assert_eq!(
            resp,
            vec![
                cocktail_a().with_ready(repo),
                cocktail_b().with_ready(repo),
            ]
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn it_should_list_only_ready_cocktails(repo: &mut dyn Repository) {
        repo.insert(cocktail_a()).unwrap();
        repo.insert(cocktail_b()).unwrap();

        repo.set_stocked(&Ingredient::from("bar"), true).unwrap();
        repo.set_stocked(&Ingredient::from("baz"), true).unwrap();

        let resp = ListCocktails::exec(repo, Request { ready_only: true }).unwrap();

        assert_eq!(resp, vec![cocktail_b().with_ready(repo)]);
    }
}
