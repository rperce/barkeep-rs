use std::collections::HashMap;

use crate::domain::{
    entities::Ingredient,
    repository::{Repository, StockError},
};

use super::ActionMut;

pub type Request = HashMap<Ingredient, bool>;

#[derive(Debug)]
pub enum Error {
    NotFound,
}

pub struct UpdateStock;
impl ActionMut for UpdateStock {
    type Request = Request;
    type Response = ();
    type Error = Error;

    fn exec_mut(
        repo: &mut Box<dyn Repository>,
        req: Self::Request,
    ) -> Result<Self::Response, Self::Error> {
        for (ingredient, have) in req {
            repo.set_stocked(&ingredient, have).map_err(|e| match e {
                StockError::NotFound => Error::NotFound,
            })?
        }

        Ok(())
    }
}
