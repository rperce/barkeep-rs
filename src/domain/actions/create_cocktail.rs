use crate::domain::actions::ActionMut;
use crate::domain::entities::{Cocktail, CocktailName, RecipeEntry};
use crate::domain::repository::{InsertError, Repository};
use serde::Deserialize;

#[derive(Default, Deserialize, Clone)]
pub struct Request {
    pub name: String,
    pub instructions: Option<String>,
    pub recipe: Option<Vec<RecipeEntry>>,
}

#[derive(Debug, PartialEq)]
pub enum Error {
    Conflict,
}

pub struct CreateCocktail;
impl ActionMut for CreateCocktail {
    type Request = Request;
    type Response = ();
    type Error = Error;

    fn exec_mut(repo: &mut Box<dyn Repository>, req: Request) -> Result<Self::Response, Error> {
        let instructions = req.instructions.unwrap_or(String::from(""));
        let recipe = req.recipe.unwrap_or(vec![]);

        let cocktail = Cocktail::new(CocktailName::from(req.name), instructions, recipe);

        repo.insert(cocktail).map_err(|e| match e {
            InsertError::Conflict => Error::Conflict,
        })?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::adapters::{InMemoryRepository, SQLiteRepository};
    use crate::domain::entities::{Ingredient, IngredientType, Measure};
    use test_case::test_case;

    fn mem_repo() -> Box<dyn Repository> {
        Box::new(InMemoryRepository::new())
    }

    fn sql_repo() -> Box<dyn Repository> {
        Box::new(SQLiteRepository::new(":memory:"))
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn it_should_return_the_recipe_created(repo: &mut Box<dyn Repository>) {
        let req = Request {
            name: String::from("Foo"),
            instructions: Some(String::from("do thing")),
            ..Default::default()
        };

        CreateCocktail::exec_mut(repo, req).unwrap();

        let c = repo.get_cocktail(&CocktailName::from("Foo")).unwrap();

        assert_eq!(c.instructions, "do thing");
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn it_should_accept_some_ingredients(repo: &mut Box<dyn Repository>) {
        let req = Request {
            name: String::from("Vodka soda"),
            instructions: Some(String::from("Stir.")),
            recipe: Some(vec![
                RecipeEntry::from((
                    Ingredient::from("Vodka"),
                    IngredientType::Measured,
                    Measure::from("1 part"),
                )),
                RecipeEntry::from((
                    Ingredient::from("Soda"),
                    IngredientType::Measured,
                    Measure::from("4 parts"),
                )),
            ]),
        };

        CreateCocktail::exec_mut(repo, req).unwrap();
    }

    #[test_case(&mut mem_repo() ; "InMemoryRepository")]
    #[test_case(&mut sql_repo() ; "SQLiteRepository")]
    fn it_should_return_an_error_when_a_cocktail_with_that_id_already_exists(
        repo: &mut Box<dyn Repository>,
    ) {
        CreateCocktail::exec_mut(
            repo,
            Request {
                name: String::from("foo bar"),
                ..Default::default()
            },
        )
        .unwrap();

        let res = CreateCocktail::exec_mut(
            repo,
            Request {
                name: String::from("Foo Bar"),
                instructions: Some(String::from("different")),
                ..Default::default()
            },
        );

        assert_eq!(res, Err(Error::Conflict));
    }
}
