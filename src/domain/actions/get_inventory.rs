use crate::domain::repository::{Inventory, Repository};

use super::Action;

pub struct Request {
    pub ready_only: bool,
}

pub type Response = Vec<Inventory>;

pub struct GetInventory;
impl Action for GetInventory {
    type Request = Request;
    type Response = Response;
    type Error = ();

    fn exec(repo: &dyn Repository, req: Self::Request) -> Result<Self::Response, Self::Error> {
        let mut ingredients = repo
            .inventory(req.ready_only)
            .into_iter()
            .map(|each| each.clone())
            .collect::<Vec<Inventory>>();

        ingredients.sort_unstable_by(|a, b| a.ingredient.0.cmp(&b.ingredient.0));
        Ok(ingredients)
    }
}

#[cfg(test)]
mod test {
    use test_case::test_case;

    use crate::adapters::{InMemoryRepository, SQLiteRepository};
    use crate::domain::entities::RecipeEntry;
    use crate::domain::{
        entities::{Cocktail, Ingredient, IngredientType},
        repository::Repository,
    };

    use super::*;

    fn cocktail_a() -> Cocktail {
        Cocktail::new(
            "a",
            "",
            vec![
                RecipeEntry::from(("foo", IngredientType::Muddled)),
                RecipeEntry::from(("bar", IngredientType::ToFill)),
            ],
        )
    }

    fn cocktail_b() -> Cocktail {
        Cocktail::new(
            "b",
            "",
            vec![RecipeEntry::from(("baz", IngredientType::ToFill))],
        )
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn it_should_list_all_the_ingredients_of_cocktails(repo: &mut dyn Repository) {
        repo.insert(cocktail_a()).unwrap();
        repo.insert(cocktail_b()).unwrap();

        let resp = GetInventory::exec(repo, Request { ready_only: false }).unwrap();

        assert_eq!(
            resp,
            vec![
                Inventory {
                    stocked: false,
                    ingredient: Ingredient::from("bar")
                },
                Inventory {
                    stocked: false,
                    ingredient: Ingredient::from("baz")
                },
                Inventory {
                    stocked: false,
                    ingredient: Ingredient::from("foo")
                },
            ]
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn it_should_list_only_ready_ingredients(repo: &mut dyn Repository) {
        repo.insert(cocktail_a()).unwrap();
        repo.insert(cocktail_b()).unwrap();

        repo.set_stocked(&Ingredient::from("bar"), true).unwrap();
        repo.set_stocked(&Ingredient::from("baz"), true).unwrap();

        let resp = GetInventory::exec(repo, Request { ready_only: true }).unwrap();

        assert_eq!(
            resp,
            vec![
                Inventory {
                    stocked: true,
                    ingredient: Ingredient::from("bar")
                },
                Inventory {
                    stocked: true,
                    ingredient: Ingredient::from("baz")
                },
            ]
        );
    }
}
