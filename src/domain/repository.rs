use serde::{Deserialize, Serialize};
use std::collections::HashSet;

use crate::domain::entities::Ingredient;
use crate::domain::entities::RecipeEntry;
use crate::domain::entities::{Cocktail, CocktailName};

#[derive(Debug, PartialEq, Eq)]
pub enum InsertError {
    Conflict,
}

#[derive(Debug, PartialEq, Eq)]
pub enum GetError {
    NotFound,
}

#[derive(Debug, PartialEq, Eq)]
pub enum ReplaceError {
    NotFound,
    Conflict,
}

#[derive(Debug, PartialEq, Eq)]
pub enum StockError {
    NotFound,
}

#[derive(Debug, PartialEq, Eq)]
pub enum AliasError {
    NotFound,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct Inventory {
    pub stocked: bool,
    pub ingredient: Ingredient,
}
impl Ingredient {
    pub fn stocked(&self, repo: &dyn Repository) -> Inventory {
        Inventory {
            stocked: repo.is_stocked(&self),
            ingredient: self.clone(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct CocktailWithReady {
    pub ready: bool,
    pub name: CocktailName,
    pub instructions: String,
    pub recipe: Vec<RecipeEntry>,
}
impl Cocktail {
    pub fn with_ready(&self, repo: &dyn Repository) -> CocktailWithReady {
        CocktailWithReady {
            name: self.name.clone(),
            recipe: self.recipe.clone(),
            instructions: self.instructions.clone(),
            ready: repo.is_ready(&self.name),
        }
    }
}

pub trait Repository: Sync + Send {
    fn get_cocktail(&self, name: &CocktailName) -> Result<CocktailWithReady, GetError>;
    fn inventory(&self, ready_only: bool) -> HashSet<Inventory>;
    fn cocktails(&self, ready_only: bool) -> HashSet<CocktailWithReady>;
    fn is_stocked(&self, ingredient: &Ingredient) -> bool;
    fn is_ready(&self, name: &CocktailName) -> bool;
    fn insert(&mut self, c: Cocktail) -> Result<(), InsertError>;
    fn replace(&mut self, name: &CocktailName, c: Cocktail) -> Result<(), ReplaceError>;
    fn set_stocked(&mut self, ingredient: &Ingredient, have: bool) -> Result<(), StockError>;
    fn alias(
        &mut self,
        ingredient: &Ingredient,
        stock_source: &Ingredient,
    ) -> Result<(), AliasError>;
}

#[cfg(test)]
mod test {
    use test_case::test_case;

    use super::*;
    use crate::adapters::{InMemoryRepository, SQLiteRepository};
    use crate::domain::entities::{Ingredient, IngredientType, RecipeEntry};

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn inserted_cocktails_can_be_gotten(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new("Foo", "test", vec![])).unwrap();
        let out = repo.get_cocktail(&CocktailName::from("Foo")).unwrap();
        assert_eq!(out.name, CocktailName::from("Foo"));
        assert_eq!(out.instructions, String::from("test"));
        assert_eq!(out.recipe, vec![]);
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn ingredients_are_those_in_cocktails(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("ing1", IngredientType::Muddled))],
        ))
        .unwrap();

        repo.insert(Cocktail::new(
            "Bar",
            "",
            vec![
                RecipeEntry::from(("ing1", IngredientType::Muddled)),
                RecipeEntry::from(("ing2", IngredientType::ToFill)),
                RecipeEntry::from(("lemon wedge", IngredientType::Garnish)),
            ],
        ))
        .unwrap();

        let ids = repo
            .inventory(false)
            .iter()
            .map(|inv| inv.ingredient.0.clone())
            .collect::<HashSet<String>>();

        assert_eq!(
            ids,
            ["ing1", "ing2", "lemon wedge"]
                .iter()
                .map(|e| String::from(*e))
                .collect::<HashSet<String>>()
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn new_ingredients_are_not_in_stock(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("ing1", IngredientType::Muddled))],
        ))
        .unwrap();

        assert_eq!(repo.is_stocked(&Ingredient::from("ing1")), false);
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn ingredients_can_be_stocked(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("ing1", IngredientType::Muddled))],
        ))
        .unwrap();

        let ing = Ingredient::from("ing1");
        repo.set_stocked(&ing, true).unwrap();
        assert_eq!(repo.is_stocked(&ing), true);
        assert_eq!(repo.is_stocked(&Ingredient::from("ing1")), true);
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    // TODO: aliasing for sqlite repository
    fn ingredients_can_be_aliased(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![
                RecipeEntry::from(("ing1", IngredientType::ToFill)),
                RecipeEntry::from(("ing2", IngredientType::ToFill)),
            ],
        ))
        .unwrap();

        let ing1 = Ingredient::from("ing1");
        let ing2 = Ingredient::from("ing2");

        repo.set_stocked(&ing2, true).unwrap();
        assert_eq!(repo.is_stocked(&ing1), false);
        assert_eq!(repo.is_stocked(&ing2), true);

        repo.alias(&ing1, &ing2).unwrap();
        assert_eq!(repo.is_stocked(&ing1), true);
        assert_eq!(repo.is_stocked(&ing2), true);
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn nonexistent_get_should_error(repo: &mut dyn Repository) {
        assert_eq!(
            repo.get_cocktail(&CocktailName::from("any")),
            Err(GetError::NotFound)
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn insert_with_same_id_should_conflict(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new("Foo Bar", "", vec![])).unwrap();

        let res = repo.insert(Cocktail::new("fOo BaR", "", vec![]));

        assert_eq!(res, Err(InsertError::Conflict));
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn nonexistent_ingredients_should_not_be_stockable(repo: &mut dyn Repository) {
        assert_eq!(
            repo.set_stocked(&Ingredient::from("any"), true),
            Err(StockError::NotFound)
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    // TODO: aliasing for sqlite repository
    fn nonexistent_ingredients_should_not_be_aliasable(repo: &mut dyn Repository) {
        let ing1 = Ingredient::from("ing1");
        let ing2 = Ingredient::from("ing2");
        assert_eq!(repo.alias(&ing1, &ing2), Err(AliasError::NotFound));
        assert_eq!(repo.alias(&ing2, &ing1), Err(AliasError::NotFound));

        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("ing1", IngredientType::ToFill))],
        ))
        .unwrap();
        assert_eq!(repo.alias(&ing1, &ing2), Err(AliasError::NotFound));
        assert_eq!(repo.alias(&ing2, &ing1), Err(AliasError::NotFound));
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn inserting_same_ingredient_in_different_cocktails_should_not_duplicate(
        repo: &mut dyn Repository,
    ) {
        repo.insert(Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::ToFill))],
        ))
        .unwrap();

        repo.insert(Cocktail::new(
            "Bar",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::Garnish))],
        ))
        .unwrap();

        assert_eq!(repo.inventory(false).len(), 1);
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn cocktails_can_be_ready(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Glass o' Water",
            "",
            vec![RecipeEntry::from(("Water", IngredientType::ToFill))],
        ))
        .unwrap();

        repo.set_stocked(&Ingredient::from("Water"), true).unwrap();

        assert_eq!(repo.is_ready(&CocktailName::from("Glass o' Water")), true);

        assert_eq!(
            repo.get_cocktail(&CocktailName::from("Glass o' Water"))
                .unwrap()
                .ready,
            true
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn cocktails_can_be_not_ready(repo: &mut dyn Repository) {
        repo.insert(Cocktail::new(
            "Glass o' Water",
            "",
            vec![RecipeEntry::from(("Water", IngredientType::ToFill))],
        ))
        .unwrap();

        assert_eq!(repo.is_ready(&CocktailName::from("Glass o' Water")), false);

        assert_eq!(
            repo.get_cocktail(&CocktailName::from("Glass o' Water"))
                .unwrap()
                .ready,
            false
        );
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn all_cocktails_can_be_gotten(repo: &mut dyn Repository) {
        let foo = 
        Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::ToFill))],
        );

        let bar =
        Cocktail::new(
            "Bar",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::Garnish))],
        );

        let water =
        Cocktail::new(
            "Glass o' Water",
            "",
            vec![RecipeEntry::from(("Water", IngredientType::ToFill))],
        );

        repo.insert(foo.clone()).unwrap();
        repo.insert(bar.clone()).unwrap();
        repo.insert(water.clone()).unwrap();
        repo.set_stocked(&Ingredient::from("Foo"), true).unwrap();

        assert_eq!(repo.cocktails(false), HashSet::from([foo.with_ready(repo), bar.with_ready(repo), water.with_ready(repo)]));
    }

    #[test_case(&mut InMemoryRepository::new() ; "InMemoryRepository")]
    #[test_case(&mut SQLiteRepository::new(":memory:") ; "SQLiteRepository")]
    fn all_ready_cocktails_can_be_gotten(repo: &mut dyn Repository) {
        let foo = 
        Cocktail::new(
            "Foo",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::ToFill))],
        );

        let bar =
        Cocktail::new(
            "Bar",
            "",
            vec![RecipeEntry::from(("Foo", IngredientType::Garnish))],
        );

        let water =
        Cocktail::new(
            "Glass o' Water",
            "",
            vec![RecipeEntry::from(("Water", IngredientType::ToFill))],
        );

        repo.insert(foo.clone()).unwrap();
        repo.insert(bar.clone()).unwrap();
        repo.insert(water.clone()).unwrap();
        repo.set_stocked(&Ingredient::from("Foo"), true).unwrap();

        assert_eq!(repo.cocktails(true), HashSet::from([foo.with_ready(repo), bar.with_ready(repo)]));
    }
}
