pub fn canonicalize(str: &str) -> String {
    return str.to_lowercase().replace(" ", "_");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_should_lowercase_characters() {
        assert_eq!("foobar", canonicalize("FoObAr"));
    }

    #[test]
    fn it_should_replace_spaces_with_underscores() {
        assert_eq!("foo_bar_baz", canonicalize("foo bar baz"));
    }
}
