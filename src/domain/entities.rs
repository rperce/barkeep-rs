use crate::domain::utils::canonicalize;
use serde::{Serialize, Deserialize};
use std::fmt;
use std::convert::TryFrom;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct Ingredient(pub String);

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub enum IngredientType {
    Measured,
    Muddled,
    ToFill,
    Garnish,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash, Deserialize, Serialize)]
pub struct Measure(pub String);

#[derive(Debug, PartialEq, Clone, Eq, Hash, Deserialize, Serialize)]
pub struct RecipeEntry {
    pub ingredient: Ingredient,
    pub r#type: IngredientType,
    pub measure: Option<Measure>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Deserialize, Serialize)]
pub struct CocktailName(pub String);
impl CocktailName {
    pub fn canonicalize(&self) -> CocktailName {
        CocktailName::from(canonicalize(&self.0.clone()))
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Deserialize, Serialize)]
pub struct Cocktail {
    pub name: CocktailName,
    pub instructions: String,
    pub recipe: Vec<RecipeEntry>,
}

impl From<String> for Ingredient {
    fn from(s: String) -> Self {
        Self(s)
    }
}
impl From<&str> for Ingredient {
    fn from(s: &str) -> Self {
        Self(s.to_string())
    }
}

impl TryFrom<String> for IngredientType {
    type Error = String;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        match s.as_str() {
            "Measured" => Ok(IngredientType::Measured),
            "Muddled" => Ok(IngredientType::Muddled),
            "ToFill" => Ok(IngredientType::ToFill),
            "Garnish" => Ok(IngredientType::Garnish),
            _ => Err(String::from("String not one of Measured|Muddled|ToFill|Garnish")),
        }
    }
}

impl From<String> for Measure {
    fn from(s: String) -> Self {
        Self(s)
    }
}
impl From<&str> for Measure {
    fn from(s: &str) -> Self {
        Self(s.to_string())
    }
}

impl From<String> for CocktailName {
    fn from(s: String) -> Self {
        Self(s)
    }
}
impl From<&str> for CocktailName {
    fn from(s: &str) -> Self {
        Self(s.to_string())
    }
}

impl fmt::Display for Ingredient {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl fmt::Display for IngredientType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl<T: Into<Ingredient>> From<(T, IngredientType, Option<Measure>)> for RecipeEntry {
    fn from((ing, r#type, measure): (T, IngredientType, Option<Measure>)) -> Self {
        Self {
            ingredient: ing.into(),
            r#type,
            measure,
        }
    }
}
impl<T: Into<Ingredient>> From<(T, IngredientType, Measure)> for RecipeEntry {
    fn from((ing, r#type, measure): (T, IngredientType, Measure)) -> Self {
        Self {
            ingredient: ing.into(),
            r#type,
            measure: Some(measure),
        }
    }
}

impl<T: Into<Ingredient>> From<(T, IngredientType)> for RecipeEntry {
    fn from((ing, r#type): (T, IngredientType)) -> Self {
        Self {
            ingredient: ing.into(),
            r#type,
            measure: None,
        }
    }
}

impl Cocktail {
    pub fn new<T: Into<CocktailName>, U: Into<String>>(
        name: T,
        instructions: U,
        recipe: Vec<RecipeEntry>,
    ) -> Self {
        Self {
            name: name.into(),
            instructions: instructions.into(),
            recipe,
        }
    }
}
