## Abstracts

The core entities we care about are `Ingredient`s and the `Cocktail`s made out of them.
We need to be able to tell which `Ingredient`s are in stock and, thus,
which `Cocktails` are ready.

## Web Strategy

The frontend is client-side rendered preact.
Server-side templates embed the data a page needs to render.
All our frontend files use typescript.

## Architecture

We're in a "hexagonal" architecture, but not strict about it.
Note the following important directories:

.
├── docs
│   └── dev-focused documentation
├── cypress
│   └── integration tests that load pages in an actual browser
├── js
│   └── frontend code
├── migrations
│   └── database migrations
├── public
│   ├── favicon/css assets (raw) and compiled js
│   └── js
│       └── compiled js from js/, gitignored
├── src
│   ├── adapters
│   │   └── interfaces with the "outside world": repositories and webservers
│   ├── domain
│   │   ├── actions
│   │   │   └── aka "use cases". input adapters should use these to do their work.
│   │   │       one file per action.
│   │   └── entities.rs: core entities, no knowledge of actions done on them or adapters
│   │                    they're stored in / accessed via
│   └── main.rs: starts the webserver
└── templates
.   └── simple handlebars templates for html files

## Operational Tasks

### Provision a new admin user

Load https://localhost:8000/signup _from a local IP_. For your local dev, passwords
still must have a capital letter and number: I recommend `P4ssword`.
