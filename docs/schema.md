# diagram

```
|  /-------------------------\
|  | cocktail                |
|  |-------------------------|
|  | *id            text     |
|  |  name          text  nn |------\
|  |  instructions  text     |      |
|  \-------------------------/      |
|                                   |         /------------------------\
|  /-------------\                  |         | recipe_entry           |
|  | ingredient  |                  |         |------------------------|
|  |-------------|                  \---------|  cocktail    text   nn |
|  | *name  text |------------T---------------|  ingredient  text   nn |
|  \-------------/            |     /---------|  type        text   nn |
|                             |     |         |  measure     text      |
|      /-------------------\  |     |         \------------------------/
|      | stock             |  |     |
|      |-------------------|  |     |
|      |  ingredient  text |--/     |
|      \-------------------/        |
|                                   |
|  /-----------------\              |
|  | ingredient_type |              |
|  |-----------------|              |
|  | *id  text       |--------------/
|  \-----------------/
```

## substitutions
/-    ┌─
\\-   └─
-\\   ─┐
-/    ─┘
-|-   ─┼─
|-    ├─
-|    ─┤
-     ─
|     │
\*    ★
T     ┬

## vimcmds
first, put cursor on this line and type j"qyy
mzggjv/## subj11@w'zk

then, to display: put cursor on this line and type j"wyy@q
0mx"eyt w"rywgv:s,e,r,ge'x

or, to edit: put cursor on this line and type j"wyy@q
0mxw"eyw0"ryt gv:s,e,r,ge'x
