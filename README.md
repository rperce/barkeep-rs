# barkeep

![a screenshot of barkeep](docs/screenshot.png)

Record the cocktails you like to make, mark which of their ingredients you have in
stock, and end up with a nice menu for yourself and your guests.

## Deploying barkeep

My personal cocktails are visible at https://barkeep.rperce.net/. To run your own
instance, you'll need a working Rust development environment running the stable
toolchain, and a reasonably modern node installation with `yarn`. Run:

```
./Taskfile build:release
```

This will generate a binary in `target/release/` called `barkeep`. This binary needs to
be run in a directory containing the `templates` and `public` directories included in
this repo, the latter of which will have been augmented with transpiled frontend
javascript files. Provide, as the first command-line arg, the path to the SQLite
database you wish to use, which will be created if it does not exist. If you don't pass
a database, it will use an ephemeral in-memory database which is handy for integration
testing but almost completely useless for actual usage.

"Edit" functionality (creating and editing cocktails, and editing stocked ingredients)
is only accessible to users inside your home network, determined by the value of the
`X-Real-IP` header. Run `barkeep` behind an `nginx`, `httpd`, or similar that sets that
header. To change what subnets count as "local", change the `local_ip` function in
[src/adapters/webserver.rs](./src/adapters/webserver.js) and recompile.

You can see how I deploy this to my home server with the `deploy` function in
[Taskfile](./Taskfile). Adjust this to your liking. The systemd unit file I use on the
server is:

```
[Unit]
Description=Cocktail Ingredient Service
After=network.target

[Service]
Type=simple
WorkingDirectory=/barkeep
ExecStart=/barkeep/barkeep /barkeep/data.db
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

## Contributing

See [docs/development.md](./docs/development.md) for more information. Feel free to file
Issues with bug reports and/or feature requests!
