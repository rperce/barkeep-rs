import { Col } from 'jsxstyle/preact'
import { h } from 'preact'
import { App } from './components/App'
import { Navbar } from './components/Navbar'
import { render } from './util/loadPage'

const SignupPage = () => {
  return (
    <App>
      <header>
        <Navbar />
        <h1>Signup</h1>
      </header>
      <Col width="fit-content" marginBottom="1rem">
        <form action="/signup" method="post">
          <label>
            Email:
            <input type="text" name="email" />
          </label>
          <label>
            Password:
            <input type="password" name="password" />
          </label>
          <input type="submit">Sign up</input>
        </form>
      </Col>
    </App>
  );
}


render(<SignupPage />);
