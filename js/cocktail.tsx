import { Block, Col, Row } from 'jsxstyle/preact'
import { h } from 'preact'
import { App } from './components/App'
import { CocktailImage } from './components/cute_image'
import { Link } from './components/Link'
import { Navbar } from './components/Navbar'
import { RecipeEntry } from './models/entities'
import { useCocktail } from './util/hooks'
import { render } from './util/loadPage'

const Entry = ({ entry }: { entry: RecipeEntry }) => {
  switch (entry.type) {
    case 'Measured':
    case 'Muddled':
      return (
        <li>
          <span>{entry.measure} </span>
          <span>
            <Link to={entry.ingredient} />
          </span>
        </li>
      )
    case 'ToFill':
      return (
        <li>
          <span>
            <Link to={entry.ingredient} />
          </span>
          <span>, to fill</span>
        </li>
      )
    case 'Garnish':
      return (
        <li>
          <span>Garnish with </span>
          <span>
            <Link to={entry.ingredient} />
          </span>
        </li>
      )
  }
}

const CocktailPage = () => {
  const cocktail = useCocktail()

  return (
    <App>
      <header>
        <Navbar />
        <h1>{cocktail.name.toString()}</h1>
        <hr />
      </header>
      <Col width="fit-content" marginBottom="1rem">
        <Row
          alignItems="center"
          marginBottom="1em"
          width="fit-content"
          maxWidth="800px"
          flexWrap="wrap"
        >
          <Block
            mediaQueries={{
              sm: 'screen and (max-width: 800px)',
            }}
            smWidth="100%"
            smMarginBottom="1rem"
          >
            <CocktailImage size={180} cocktail={cocktail} />
          </Block>
          <ul>
            {cocktail.recipe.map((each, i) => (
              <Entry key={i} entry={each} />
            ))}
          </ul>
        </Row>
        {cocktail.instructions ? (
          <Block display="flex">
            <Block width="0" flexGrow={1} id="instructions">
              {cocktail.instructions}
            </Block>
          </Block>
        ) : null}
      </Col>
      <a href={`/cocktail/${cocktail.name}/edit`}>Edit</a>
    </App>
  )
}

render(<CocktailPage />)
