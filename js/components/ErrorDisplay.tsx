import { Block } from 'jsxstyle/preact'
import { h, JSX } from 'preact'

export const ErrorDisplay = ({ error }: { error: string | undefined }): JSX.Element | null => {
  if (error === undefined) return null

  return (
    <Block fontSize="large" padding="1rem" color="white" backgroundColor="#b00020">
      Error: {error.toString()}
    </Block>
  )
}
