import { Col } from 'jsxstyle/preact'
import { FunctionalComponent, h } from 'preact'

export const App: FunctionalComponent = ({ children, ...rest }) => (
  <Col maxWidth="800px" marginLeft="auto" marginRight="auto" paddingTop="1rem" width="90%" {...rest}>
    {children}
  </Col>
)
