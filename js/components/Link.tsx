import { h } from 'preact'
import { Cocktail, Ingredient } from '../models/entities'

export const Link = ({ to }: { to: Ingredient | Cocktail }) => {
  const path = to instanceof Cocktail ? 'cocktail' : 'ingredient'
  return <a href={`/${path}/${encodeURIComponent(to.name.toString())}`}>{to.name.toString()}</a>
}
