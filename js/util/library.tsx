import { Block } from 'jsxstyle/preact'
import { FunctionalComponent, h } from 'preact'

export const Ul: FunctionalComponent = props => <Block component="ul" padding="none" {...props} />
export const Li: FunctionalComponent = props => <Block component="li" display="list-item" {...props} />
