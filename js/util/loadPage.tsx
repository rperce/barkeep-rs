import { render as preactRender, JSX } from 'preact'
export const render = (component: JSX.Element): void => {
  const content = document.getElementById('content')
  if (content) {
    preactRender(component, content)
  } else {
    preactRender(<div>Error: no element with id 'content'</div>, document.body)
  }
}
