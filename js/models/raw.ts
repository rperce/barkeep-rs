// types should be based on the structs in src/domain/repository.rs

export type IngredientObj = string

export type InventoryObj = {
  stocked: boolean
  ingredient: string
}

export type RecipeEntryObj = {
  ingredient: string
  type: string
  measure?: string
}

export type CocktailObj = {
  ready: boolean
  name: string
  instructions: string
  recipe: Array<RecipeEntryObj>
}
