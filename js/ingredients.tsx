import { Col } from 'jsxstyle/preact'
import { h } from 'preact'
import * as hooks from 'preact/hooks'
import { Link } from './components/Link'
import { Navbar } from './components/Navbar'
import { Cocktail, RecipeEntry } from './models/entities'
import { useCocktails, useInventory } from './util/hooks'
import { render } from './util/loadPage'

const IngredientsPage = () => {
  const inventory = useInventory()
  const cocktails = useCocktails()
  const ingredientMap = hooks.useMemo(() => {
    const ingToSet = new Map<string, Set<Cocktail>>()
    cocktails.forEach((cocktail: Cocktail) => {
      cocktail.recipe.forEach((entry: RecipeEntry) => {
        const name = entry.ingredient.name
        const set = ingToSet.get(name) || new Set<Cocktail>()
        set.add(cocktail)
        ingToSet.set(name, set)
      })
    })

    return new Map<string, Array<Cocktail>>(
      Array.from(ingToSet, ([ing, set]: [ing: string, set: Set<Cocktail>]) => [
        ing,
        Array.from(set).sort(),
      ])
    )
  }, [inventory, cocktails])

  const [stockMap, setStockMap] = hooks.useState(() =>
    Object.fromEntries(inventory.map(each => [each.ingredient.name, each.stocked]))
  )

  const updateStockMap = hooks.useCallback(
    (ing: string, evt: Event) => {
      if (evt.target instanceof HTMLInputElement) {
        const target: HTMLInputElement = evt.target
        setStockMap(map => ({ ...map, [ing]: target.checked }))
      }
    },
    [setStockMap]
  )

  const onSubmit = hooks.useCallback(() => {
    fetch('/ingredients', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(stockMap),
    })
  }, [stockMap])

  return (
    <Col>
      <header>
        <Navbar />
        <h1>Ingredients</h1>
        <hr />
      </header>
      <table id="ingredients">
        <thead>
          <tr>
            <th>Ingredient</th>
            <th>Stocked?</th>
            <th>Used in</th>
          </tr>
        </thead>
        <tbody>
          {Object.entries(stockMap).map(([name, stocked]: [string, boolean]) => {
            return (
              <tr key={name}>
                <td>{name}</td>
                <td>
                  {window.logged_in ? (
                    <input
                      type="checkbox"
                      disabled={!window.logged_in}
                      checked={stocked}
                      onChange={(evt: Event) => {
                        updateStockMap(name, evt)
                      }}
                    />) : stocked ? '✓' : null
                  }
                </td>
                <td>
                  {ingredientMap.get(name)?.map(c => (
                    <span key={name + c.name.name} className="commasep">
                      <Link to={c} />
                    </span>
                  )) || 'Nothing'}
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
      <button type="button" onClick={onSubmit}>
        Save
      </button>
    </Col>
  )
}

render(<IngredientsPage />)
