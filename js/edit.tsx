import { Col } from 'jsxstyle/preact'
import { h, JSX } from 'preact'
import * as hooks from 'preact/hooks'
import { ErrorDisplay } from './components/ErrorDisplay'
import { Navbar } from './components/Navbar'
import { Cocktail } from './models/entities'
import { RecipeEntryObj } from './models/raw'
import { useCocktail, useEventSetState } from './util/hooks'
import { render } from './util/loadPage'

const RecipeEntryTypeDropdown = ({
  value,
  onChange,
}: {
  value: string
  onChange: (a: Event) => void
}) => {
  return (
    <select name="type" {...{ value, onChange }}>
      <option value="Measured">Measured</option>
      <option value="Muddled">Muddled</option>
      <option value="ToFill">To Fill</option>
      <option value="Garnish">Garnish</option>
    </select>
  )
}

type EntryProps = {
  entry: RecipeEntryObj
  setEntry: (a: RecipeEntryObj) => void
  delEntry: () => void
}
const Entry = ({ entry, setEntry, delEntry }: EntryProps): JSX.Element => {
  const [type, setType] = useEventSetState(entry.type)
  const [measure, setMeasure] = useEventSetState(entry.measure || '')
  const [name, setName] = useEventSetState(entry.ingredient)

  hooks.useEffect(() => {
    setEntry({ ingredient: name, type, measure: measure === '' ? undefined : measure })
  }, [name, measure, type])

  return (
    <li>
      <fieldset>
        <RecipeEntryTypeDropdown value={type} onChange={setType} />
        {type === 'Measured' || type === 'Muddled' ? (
          <input name="measure" value={measure} onInput={setMeasure} />
        ) : null}
        <input aria-label="name" name="name" value={name} onInput={setName} />
        <button aria-label="delete" type="button" onClick={delEntry}>
          ×
        </button>
      </fieldset>
    </li>
  )
}

type Props = {
  cocktail: Cocktail
  generateRequestBody: (
    name: string,
    instructions: string,
    recipe: Array<RecipeEntryObj>
  ) => Record<string, unknown>
  requestPath: string
}
const AddOrEditPage = ({ cocktail, generateRequestBody, requestPath }: Props) => {
  const [name, setName] = useEventSetState(cocktail.name.toString())
  const [instructions, setInstructions] = useEventSetState(cocktail.instructions)
  const [recipe, setRecipe] = hooks.useState(cocktail.recipe.map(each => each.toObj()))

  const setNthRecipeEntry = (n: number) => (newEntry: RecipeEntryObj) =>
    setRecipe(recipe =>
      newEntry === recipe[n] ? recipe : [...recipe.slice(0, n), newEntry, ...recipe.slice(n + 1)]
    )

  const [deleteCount, setDeleteCount] = hooks.useState(0)
  const delNthRecipeEntry = (n: number) => () => {
    setRecipe(recipe => [...recipe.slice(0, n), ...recipe.slice(n + 1)])
    setDeleteCount(n => n + 1)
  }

  const addRecipeEntry = () =>
    setRecipe(r => [...r, { ingredient: '', type: 'Measured', measure: '' }])

  const [error, setError] = hooks.useState<string | undefined>(undefined)

  const onSubmit = hooks.useCallback(() => {
    fetch(requestPath, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(generateRequestBody(name, instructions, recipe)),
    }).then(resp => {
      if (!resp.ok) {
        resp.text().then(text => {
          setError(text)
        })
      }
    })
  }, [generateRequestBody, name, instructions, recipe])

  return (
    <Col>
      <header>
        <Navbar />
        <h1>
          Name: <input value={name} onInput={setName} />
        </h1>
        <hr />
      </header>
      <ErrorDisplay error={error} />
      <h3><label for="recipe">Ingredients:</label></h3>
      <ul id="recipe">
        {recipe.map((entry, i) => (
          <Entry
            key={`${deleteCount}${i}`}
            entry={entry}
            setEntry={setNthRecipeEntry(i)}
            delEntry={delNthRecipeEntry(i)}
          />
        ))}
        <li>
          <button id="add_ingredient" type="button" onClick={addRecipeEntry}>
            Add Ingredient
          </button>
        </li>
      </ul>
      <h3><label for="instructions">Instructions:</label></h3>
      <textarea id="instructions" value={instructions} onInput={setInstructions} />
      <button type="button" onClick={onSubmit}>
        Save
      </button>
    </Col>
  )
}
const EditPage = () => {
  const cocktail = useCocktail()

  const generateRequestBody = hooks.useCallback(
    (name: string, instructions: string, recipe: Array<RecipeEntryObj>) => ({
      from: cocktail.name.toString(),
      name,
      instructions,
      recipe,
    }),
    [cocktail.name.toString()]
  )

  return <AddOrEditPage {...{ cocktail, generateRequestBody }} requestPath="/cocktail/edit" />
}

const AddPage = () => {
  const cocktail = useCocktail()

  const generateRequestBody = hooks.useCallback(
    (name: string, instructions: string, recipe: Array<RecipeEntryObj>) => ({
      name,
      instructions,
      recipe,
    }),
    []
  )

  return <AddOrEditPage {...{ cocktail, generateRequestBody }} requestPath="/create_cocktail" />
}

render(window.location.href.endsWith('/create_cocktail') ? <AddPage /> : <EditPage />)
